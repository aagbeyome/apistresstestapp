const express = require('express');
const path = require('path');
const fs = require('fs');
const helmet = require('helmet');
const cors = require("cors");
const cookieParser = require('cookie-parser');
const LoadTester = require("./services/LoadTester.js")

const app = express();

const options = {
    "key": fs.readFileSync(`${__dirname}/certs/key.pem`),
    "cert": fs.readFileSync(`${__dirname}/certs/cert.pem`),
    "chain": fs.readFileSync(`${__dirname}/certs/chain.pem`)
};

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(helmet());

app.use(cors());
app.use(express.static(path.join(__dirname, 'web-dashboard/dist/web-dashboard')));

app.get('/', async (req, res) => {
    res.sendFile(path.join(__dirname, 'web-dashboard/dist/web-dashboard', 'index.html'));
});

//socket io
const server = require("http").Server(app);
const httpsServer = require('https').createServer(options, app);

let io = require("socket.io")(httpsServer); //UNDO!! httpsServer

const port = Number(process.env.PORT) || 3000;
const httpsPort = Number(process.env.HTTPSPORT || 3001)

server.listen(port, () => {
    console.log('HTTP server started on port: ' + port);
});

httpsServer.listen(httpsPort, function () {
    console.log("HTTPS Web Server started on port " + httpsPort);
})
    .on("error", function () {
        console.log("Unable to Bind HTTPS Server to port " + httpsPort);
    });


//socket
io.on("connection", (socket) => {
    console.log(`Client ${socket.id} connected`);

    socket.on("RUN", (payload) => {

        const { requestCt, requestId } = payload
        LoadTester.runRequests(requestCt, requestId, socket)
    });

    socket.on("START", (payload) => {

        //const { requestCt, requestId } = payload
        //LoadTester.runRequests(requestCt, requestId, socket)
        LoadTester.startRequests(payload, socket)
    });

    socket.on("ABORT", (requestId) => {
        LoadTester.abortRequest(requestId)
    })

});


