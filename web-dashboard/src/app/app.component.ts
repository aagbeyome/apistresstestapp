import { Component, OnInit, ViewChild } from '@angular/core';
import { SocketService } from './services/socket.service';
import { TestResponse } from './models/TestResponse';
import { FormInput } from './models/FormInput';
import Chart from 'chart.js';
import { barchartdata } from './models/barchart';
import { Time } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

export interface TimePoint {
  time: number,
  successCount: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'web-app';

  //test stats 

  chartData = {
    statuses: {},
    responseTimeCounts: {}
  }

  requestCount: number = 1000;
  responseCount: number = 0;
  avgResponseTime: number = 0;
  totalElapsedTime = 0;
  isTestRunning = false;
  totalSuccess = 0;
  totalFailures = 0;

  successTrackerTimer = null;
  startTime = Date.now()
  timeSuccessData: TimePoint[] = []
  requestResponses: TestResponse[] = []

  showmodal = false;
  modalData = new FormInput("http://172.18.0.3:7076/mockService",
    "GET",
    10, "", "",
    JSON.stringify({ "Content-type": "application/json" }), "", false);

  formData = { ...this.modalData }

  testResponseSubscription = null;
  currentRequestId = "";

  hideElement = false;


  //chart canvases
  @ViewChild("responseBarchart", { static: false }) responseBarchart: any;
  barchartCanvas: any;
  barchartData: any;
  barChartObject: any;

  @ViewChild("responseTimePieChart", { static: false }) responseTimePieChart: any;
  piechartCanvas: any;
  piechartData: any;
  pieChartObject: any;

  @ViewChild("successLineChart", { static: false }) successLineChart: any;
  linechartCanvas: any;
  linechartData: any;
  lineChartObject: any;

  constructor(private socketService: SocketService) {
    this.testResponseSubscription = this.socketService.getSocket().fromEvent<TestResponse>('testResponse');
  }

  /**
   * @name AppComponent.ngOnInit
   * @methodOf AppComponent
   * @description
   * Lifecycle event
   * @param (void)
   * @returns (void)
   */
  ngOnInit() {
    this.createSubscription()
  }

  ngAfterViewInit() {

    this.barchartCanvas = this.responseBarchart.nativeElement;
    this.barchartData = barchartdata;

    this.piechartCanvas = this.responseTimePieChart.nativeElement;
    this.piechartData = {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [0, 1], backgroundColor: [
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 99, 132, 0.2)',
          ],
          borderColor: [
            'rgba(54, 162, 235, 1)',
            'rgba(255, 99, 132, 1)',
          ],
          borderWidth: 1
        }],
        labels: ["high", "low"]
      }
    }

    this.linechartCanvas = this.successLineChart.nativeElement;
    this.linechartData = {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          data: [],
          label: "Successful Responses",
          borderColor: "#3e95cd",
          fill: false
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Success over time(s)'
        }
      }

    }

    this.drawChart()
  }

  showModal() {
    this.showmodal = true
    document.body.classList.add("noscroll")
  }

  clearModal() {
    this.formData = { ...this.modalData }
  }



  isValidData(): Boolean {
    let res = this.isValidJson(this.formData.headers) && this.isValidJson(this.formData.postdata)
    res = res && (this.formData.reqCount < 100 ||
      (this.formData.reqCount >= 100 && this.formData.url.indexOf('172.18.0.3') > 0) || this.formData.accessCheck == true)
    return res
  }

  isValidJson(datastr): Boolean {
    let chk = true
    if (datastr !== "") {
      chk = false
      try {
        chk = Boolean(JSON.parse(datastr))
      } catch (ex) {
      }
    }

    return chk
  }

  closeModal() {
    this.showmodal = false
    document.body.classList.remove("noscroll")
  }

  drawChart() {
    //bar chart
    this.barChartObject = new Chart(this.barchartCanvas, this.barchartData)

    //pie chart 
    this.pieChartObject = new Chart(this.piechartCanvas, this.piechartData)

    //line chart
    this.lineChartObject = new Chart(this.linechartCanvas, this.linechartData)
  }

  reset() {
    this.requestResponses = []
    this.timeSuccessData = []
    this.responseCount = 0;
    this.avgResponseTime = 0;
    this.totalElapsedTime = 0;
    this.totalSuccess = 0;
    this.totalFailures = 0;

    this.chartData.statuses = {}
    this.chartData.responseTimeCounts = {}

    //
    this.pieChartObject.data.datasets[0].data = []
    this.pieChartObject.data.labels = []
    this.barChartObject.data.labels = []
    this.barChartObject.data.datasets[0].data = []
    this.lineChartObject.data.labels = []
    this.lineChartObject.data.datasets[0].data = []

    this.barChartObject.update()
    this.pieChartObject.update()
    this.lineChartObject.update()
  }

  stopSubcription() {
    this.isTestRunning = false
    this.testResponseSubscription.unsubscribe();
    this.testResponseSubscription = null
  }

  createSubscription() {
    if (!this.testResponseSubscription) {
      this.stopSubcription()
    }

    this.testResponseSubscription.subscribe((testResponse) => {
      this.processResponse(testResponse)
    });

  }

  onAbortTest() {
    this.isTestRunning = false
    this.socketService.abortStressTests(this.currentRequestId)
  }

  getSuccessRate() {
    let avg = Math.round((this.totalSuccess / this.requestCount) * 100)
    return avg
  }

  getSuccessPercent() {
    return `${this.getSuccessRate()}%`
  }

  processResponse(testResponse: TestResponse) {

    this.requestResponses.push(testResponse)

    this.responseCount += 1
    this.totalElapsedTime += testResponse.timeElapsed
    this.avgResponseTime = parseFloat((this.totalElapsedTime / this.responseCount).toFixed(1))

    if (testResponse.status == 200) {
      this.totalSuccess += 1
    } else {
      this.totalFailures += 1
    }

    if (this.responseCount >= this.requestCount) {
      this.isTestRunning = false
    }

    //Reduce  data
    //1) Status Code Barchart
    if (this.chartData.statuses[testResponse.status]) {
      this.chartData.statuses[String(testResponse.status)] += 1
    } else {
      this.chartData.statuses[String(testResponse.status)] = 1
    }

    //barchartdata.data.labels / datasets[0].data
    this.barChartObject.data.labels = []
    this.barChartObject.data.datasets[0].data = []

    for (var k in this.chartData.statuses) {
      this.barChartObject.data.labels.push(k)
      this.barChartObject.data.datasets[0].data.push(this.chartData.statuses[k])
    }

    // ResponseTime Pie Chart
    this.chartData.responseTimeCounts = {}
    const midTime = Math.round(this.avgResponseTime)
    const low = `Below ${midTime}s`
    const high = `Exceeds ${midTime}s`
    this.chartData.responseTimeCounts[low] = 0
    this.chartData.responseTimeCounts[high] = 0

    this.requestResponses.forEach((item) => {
      if (item.timeElapsed <= midTime) {
        this.chartData.responseTimeCounts[low] += 1
      } else {
        this.chartData.responseTimeCounts[high] += 1
      }
    })

    this.pieChartObject.data.datasets[0].data = []
    this.pieChartObject.data.labels = []

    for (var k in this.chartData.responseTimeCounts) {
      this.pieChartObject.data.labels.push(k)
      this.pieChartObject.data.datasets[0].data.push(this.chartData.responseTimeCounts[k])
    }

    setTimeout(() => {
      this.barChartObject.update()
      this.pieChartObject.update()
    }, 2000)

  }

  getRandomId(): string {
    return Math.random().toString(36).substr(2, 10).toUpperCase();
  }

  endSuccessTracker() {
    clearInterval(this.successTrackerTimer)
    console.log(this.timeSuccessData)

    this.lineChartObject.data.labels = []
    this.lineChartObject.data.datasets[0].data = []

    this.timeSuccessData.map((item) => {
      this.lineChartObject.data.labels.push(item.time)
      this.lineChartObject.data.datasets[0].data.push(item.successCount)
    })
    this.lineChartObject.update()
  }

  startSuccessTracker() {

    this.successTrackerTimer = setInterval(() => {
      if (this.requestCount <= this.responseCount) {
        this.endSuccessTracker()
      } else {
        const time = Math.round((Date.now() - this.startTime) / 1000)

        const tPoint: TimePoint = { "time": time, "successCount": this.totalSuccess }
        this.timeSuccessData.push(tPoint)
      }

    }, 1000)
  }

  /**
   * @name AppComponent.onStartTests
   * @methodOf AppComponent
   * @description
   * Button click event handler to send request to server
   * @param (void)
   * @returns (void)
   */
  onStartTests() {
    this.requestCount = this.formData.reqCount
    if (this.requestCount > 0) {
      this.startTime = Date.now()
      this.startSuccessTracker()
      this.isTestRunning = true
      this.reset()
      this.currentRequestId = this.getRandomId()
      this.socketService.runStressTests(this.formData, this.currentRequestId)
      this.closeModal()
    }
  }

  /**
   * @name AppComponent.ngOnDestroy
   * @methodOf AppComponent
   * @description
   * cancel subscription to receive messages from websocket
   * @param (void)
   * @returns (void)
   */
  ngOnDestroy() {
    this.stopSubcription()
  }

}
