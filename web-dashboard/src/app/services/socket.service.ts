import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { FormInput } from '../models/FormInput';

enum SocketOption {
  abort = "ABORT",
  run = "RUN",
  start = "START"
}

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) {
  }

  /**
 * @name SocketService.sendMessage
 * @methodOf SocketService
 * @description
 * Dispatch message over websocket to server
 * @param {number} reqCount
 * @returns {void} msg
 */
  private sendMessage(option: string, payload: any) {
    this.socket.emit(option, payload);
  }

  /**
* @name SocketService.runStressTests
* @methodOf SocketService
* @description
* Dispatch message over websocket to server
* @param {number} reqCt
* @param {string} reqId
* @returns {void} msg
*/
  startStressTests(requestCt: number, requestId: string) {
    this.sendMessage(SocketOption.run, { requestCt, requestId })
  }

  /**
* @name SocketService.runStressTests
* @methodOf SocketService
* @description
* Dispatch message over websocket to server
* @param {FormInput} payload
* @param {string} reqId
* @returns {void} msg
*/
  runStressTests(payload: FormInput, requestId: string) {
    payload["requestId"] = requestId
    this.sendMessage(SocketOption.start, payload)
  }

  /**
* @name SocketService.abortStressTests
* @methodOf SocketService
* @description
* Dispatch message over websocket to server
* @param {string} reqId
* @returns {void} msg
*/
  abortStressTests(requestId: string) {
    this.sendMessage(SocketOption.abort, requestId)
  }

  /**
* @name SocketService.getSocket
* @methodOf SocketService
* @description
* returns handler to socket
* @param {void}
* @returns {Socket} Socket
*/
  getSocket(): Socket {
    return this.socket;
  }
}