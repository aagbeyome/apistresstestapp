const axios = require('axios').default;

var LoadTester = {
    abortedRequestIds: new Set(),
    backendUrl: process.env.MOCKSERVICE,

    runRequests: (reqCount, reqId, socket) => {

        const getRandomId = () => {
            return Math.random().toString(36).substr(2, 10).toUpperCase();
        };

        const done = (tElapsed, response, socket, reqId) => {
            if (!LoadTester.abortedRequestIds.has(reqId)) {
                tElapsed = process.hrtime(tElapsed);
                let tElapse = (parseFloat(`${tElapsed[0]}.${tElapsed[1]}`)).toFixed(3)
                let { status } = response
                if (!status) { status = 400 }

                let res = {
                    "requestId": reqId,
                    "responseId": getRandomId(),
                    "timeElapsed": parseFloat(tElapse),
                    "status": status
                }

                socket.emit("testResponse", res)
            }
        }

        const sendRequest = async (socket, reqId) => {
            console.log("..sendRequest")
            let timeElapsed = process.hrtime();
            try {
                const response = await axios.get(LoadTester.backendUrl, { timeout: 15000 });
                done(timeElapsed, response, socket, reqId)
            } catch (error) {
                console.log(error)
                done(timeElapsed, error, socket, reqId)
            }
        }

        for (let i = 0; i < reqCount; i++) {
            if (!LoadTester.abortedRequestIds.has(reqId)) {
                sendRequest(socket, reqId)
            }
        }

    },

    startRequests: (payload, socket) => {
        const getRandomId = () => {
            return Math.random().toString(36).substr(2, 10).toUpperCase();
        };

        const done = (tElapsed, response, socket, reqId) => {
            if (!LoadTester.abortedRequestIds.has(reqId)) {
                tElapsed = process.hrtime(tElapsed);
                let tElapse = (parseFloat(`${tElapsed[0]}.${tElapsed[1]}`)).toFixed(3)
                let status = 400;

                try {
                    if (response["status"]) {
                        status = response["status"]
                    }

                    if (response["response"]) {
                        if (response.response["status"]) {
                            status = response.response["status"]
                        }
                    }
                } catch (ex) {
                }


                let res = {
                    "requestId": reqId,
                    "responseId": getRandomId(),
                    "timeElapsed": parseFloat(tElapse),
                    "status": status
                }

                socket.emit("testResponse", res)
            }
        }

        const sendRequest = async (socket, reqId, payload) => {
            let { url, reqCount, method, username, password, headers, postdata } = payload

            url = url || process.env.MOCKSERVICE
            reqCount = reqCount || 10
            method = method || "GET"
            username = username || ""
            password = password || ""
            headers = JSON.parse(headers) || { "content-type": "application/json" }

            if (postdata !== "") {
                postdata = {}
                try {
                    postdata = (typeof postdata == "string" ? JSON.parse(postdata) : postdata) || {}
                } catch (ex) {
                }
            }

            let timeElapsed = process.hrtime();
            const config = {
                "method": method,
                "url": url,
                "headers": headers,
                "data": postdata,
                "timeout": 15000
            }
            axios(config)
                .then((response) => {
                    done(timeElapsed, response, socket, reqId)
                })
                .catch((err) => {
                    done(timeElapsed, err, socket, reqId)
                });
        }

        let { reqCount, requestId } = payload
        reqCount = reqCount || 10

        for (let i = 0; i < reqCount; i++) {
            if (!LoadTester.abortedRequestIds.has(requestId)) {
                sendRequest(socket, requestId, payload)
            }
        }
    },

    abortRequest: (reqId) => {
        LoadTester.abortedRequestIds.add(reqId)
    }

};

module.exports = LoadTester;