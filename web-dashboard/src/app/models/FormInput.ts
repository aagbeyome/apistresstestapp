export interface IFormInput {
    url: string,
    method: string,
    reqCount: number,
    username: string,
    password: string,
    headers: string,
    postdata: string,
    accessCheck: boolean
}

export class FormInput implements IFormInput {

    public url: string;
    public method: string;
    public reqCount: number;
    public username: string;
    public password: string;
    public headers: string;
    public postdata: string;
    public accessCheck: boolean;

    constructor(url: string,
        method: string,
        reqCount: number,
        username: string,
        password: string,
        headers: string,
        postdata: string,
        accessCheck: boolean) {
        this.url = url;
        this.method = method;
        this.reqCount = reqCount;
        this.username = username;

        this.password = password;
        this.headers = headers;
        this.postdata = postdata;
        this.accessCheck = accessCheck;
    }
};